import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('http://localhost:8080/')

WebUI.setText(findTestObject('Page_MyCollab - Online project management/input_Email_username'), 'ilalova3198@gmail.com')

WebUI.setEncryptedText(findTestObject('Page_MyCollab - Online project management/input_Password_password'), 'yoXzwIskMKny5gwvAM7sBA==')

WebUI.click(findTestObject('Page_MyCollab - Online project management/span_Log In'))

WebUI.click(findTestObject('Page_Dashboard MyCollab/div_New'))

WebUI.click(findTestObject('Page_Dashboard MyCollab/span_Project'))

WebUI.setText(findTestObject('Page_Dashboard MyCollab/input_Name_gwt-uid-20'), 'Web site development')

WebUI.setText(findTestObject('Page_Dashboard MyCollab/input__gwt-uid-22'), 'web')

WebUI.click(findTestObject('Object Repository/Page_Dashboard MyCollab/div_Status_v-filterselect-button'))

WebUI.setText(findTestObject('Page_Dashboard MyCollab/input__gwt-uid-22'), 'web')

WebUI.click(findTestObject('Page_Dashboard MyCollab/div_Leader_v-filterselect-button'))

WebUI.click(findTestObject('Page_Dashboard MyCollab/td_ilalova3198'))

WebUI.setText(findTestObject('Page_Dashboard MyCollab/input_Home Page_v-textfield v-widget v-has-_b0d279'), 'some page')

WebUI.click(findTestObject('Page_Dashboard MyCollab/input_Home Page_v-textfield v-widget v-has-_b0d279'))

WebUI.click(findTestObject('Page_Dashboard MyCollab/button_Start Date_v-datefield-button'))

WebUI.click(findTestObject('Page_Dashboard MyCollab/button_'))

WebUI.click(findTestObject('Page_Dashboard MyCollab/span_27'))

WebUI.click(findTestObject('Page_Dashboard MyCollab/button_Start Date_v-datefield-button'))

WebUI.click(findTestObject('Page_Dashboard MyCollab/button_'))

WebUI.click(findTestObject('Page_Dashboard MyCollab/span_30'))

WebUI.click(findTestObject('Page_Dashboard MyCollab/span_Save'))

WebUI.click(findTestObject('Page_Dashboard MyCollab/span_Skip'))

WebUI.closeBrowser()

