import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('http://localhost:8080/')

WebUI.setText(findTestObject('Object Repository/Page_MyCollab Setup Assistant Page/input_Server address (without port value an_77bc6d (2) (1)'), 
    'localhost')

WebUI.setText(findTestObject('Object Repository/Page_MyCollab Setup Assistant Page/input_Database name_databaseName (2) (1)'), 
    'mycollab')

WebUI.setText(findTestObject('Object Repository/Page_MyCollab Setup Assistant Page/input_User name_dbUserName (2) (1)'), 
    'root')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_MyCollab Setup Assistant Page/input_Password_dbPassword (2) (1)'), 
    'loY/1845nitA0V8S+cELIQ==')

WebUI.setText(findTestObject('Object Repository/Page_MyCollab Setup Assistant Page/input_Database server address_databaseServer (2) (1)'), 
    'localhost:3306')

WebUI.click(findTestObject('Object Repository/Page_MyCollab Setup Assistant Page/button_Check Connection (2)'))

WebUI.click(findTestObject('Object Repository/Page_MyCollab Setup Assistant Page/button_Setup (2) (1)'))

WebUI.closeBrowser()

