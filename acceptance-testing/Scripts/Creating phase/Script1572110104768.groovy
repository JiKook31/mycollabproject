import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('http://localhost:8080/')

WebUI.setText(findTestObject('Page_MyCollab - Online project management/input_Email_username (2)'), 'ilalova3198@gmail.com')

WebUI.setEncryptedText(findTestObject('Page_MyCollab - Online project management/input_Password_password (2)'), 'yoXzwIskMKny5gwvAM7sBA==')

WebUI.click(findTestObject('Page_MyCollab - Online project management/span_Log In (2)'))

WebUI.click(findTestObject('Page_Dashboard MyCollab/a_Web site development'))

WebUI.click(findTestObject('Object Repository/Page_Dashboard MyCollab/div_Phases'))

WebUI.click(findTestObject('Object Repository/Page_Roadmap MyCollab/span_New Phase'))

WebUI.setText(findTestObject('Object Repository/Page_Roadmap MyCollab/input_Name_gwt-uid-30'), 'Some phase')

WebUI.click(findTestObject('Object Repository/Page_Roadmap MyCollab/span_Assign to me'))

WebUI.click(findTestObject('Object Repository/Page_Roadmap MyCollab/div__v-filterselect-button'))

WebUI.click(findTestObject('Object Repository/Page_Roadmap MyCollab/td_Future'))

WebUI.click(findTestObject('Object Repository/Page_Roadmap MyCollab/span_Save'))

WebUI.click(findTestObject('Object Repository/Page_Roadmap MyCollab/a_Some phase'))

WebUI.click(findTestObject('Object Repository/Page_Phase Some phase MyCollab/span_Edit'))

WebUI.click(findTestObject('Object Repository/Page_Edit Phase Some phase MyCollab/span_Save'))

WebUI.click(findTestObject('Object Repository/Page_Phase Some phase MyCollab/span_'))

WebUI.click(findTestObject('Object Repository/Page_Phase Some phase MyCollab/span_Delete'))

WebUI.click(findTestObject('Object Repository/Page_Phase Some phase MyCollab/span_Yes'))

WebUI.closeBrowser()

