import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('http://localhost:8080/#project/dashboard')

WebUI.setText(findTestObject('Page_MyCollab - Online project management/input_Email_username (4)'), 'ilalova3198@gmail.com')

WebUI.setEncryptedText(findTestObject('Page_MyCollab - Online project management/input_Password_password (4)'), 'yoXzwIskMKny5gwvAM7sBA==')

WebUI.click(findTestObject('Page_MyCollab - Online project management/span_Log In (4)'))

WebUI.click(findTestObject('Page_Dashboard MyCollab/a_Web site development'))

WebUI.click(findTestObject('Object Repository/Page_Dashboard MyCollab/div_Pages'))

WebUI.click(findTestObject('Object Repository/Page_Pages MyCollab/span_New Page'))

WebUI.setText(findTestObject('Object Repository/Page_Page Details MyCollab/input_Subject_gwt-uid-21'), 'Some page')

WebUI.click(findTestObject('Object Repository/Page_Page Details MyCollab/p'))

WebUI.setText(findTestObject('Object Repository/Page_Page Details MyCollab/body_Some description'), '<p style="">Some description<br></p><div id="katalon" style="top: 355px;"><div id="katalon-rec_elementInfoDiv" style="display: none;"></div></div>')

WebUI.click(findTestObject('Object Repository/Page_Page Details MyCollab/span_Save'))

WebUI.click(findTestObject('Object Repository/Page_Page Details MyCollab/span_Edit'))

WebUI.click(findTestObject('Object Repository/Page_Page Details MyCollab/span_Save'))

WebUI.click(findTestObject('Object Repository/Page_Page Details MyCollab/span_'))

WebUI.click(findTestObject('Object Repository/Page_Page Details MyCollab/span_Delete'))

WebUI.click(findTestObject('Object Repository/Page_Page Details MyCollab/span_Yes'))

WebUI.closeBrowser()

