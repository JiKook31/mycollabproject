import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('http://localhost:8080/')

WebUI.setText(findTestObject('Page_MyCollab - Online project management/input_Email_username (5)'), 'ilalova3198@gmail.com')

WebUI.setEncryptedText(findTestObject('Page_MyCollab - Online project management/input_Password_password (5)'), 'yoXzwIskMKny5gwvAM7sBA==')

WebUI.click(findTestObject('Page_MyCollab - Online project management/span_Log In (5)'))

WebUI.click(findTestObject('Page_Dashboard MyCollab/a_Web site development (4)'))

WebUI.click(findTestObject('Object Repository/Page_Dashboard MyCollab/div_Users  Settings'))

WebUI.click(findTestObject('Object Repository/Page_Dashboard MyCollab/div_Members'))

WebUI.click(findTestObject('Object Repository/Page_Members MyCollab/a_ilalova3198'))

WebUI.click(findTestObject('Object Repository/Page_Member ilalova3198 MyCollab/div_Roles'))

WebUI.click(findTestObject('Object Repository/Page_Roles MyCollab/a_Client'))

WebUI.click(findTestObject('Object Repository/Page_Role Client MyCollab/span_Edit'))

WebUI.click(findTestObject('Object Repository/Page_Edit Role Client MyCollab/span_Save'))

WebUI.click(findTestObject('Object Repository/Page_Roles MyCollab/div_Components'))

WebUI.click(findTestObject('Object Repository/Page_Components MyCollab/div_Versions'))

WebUI.click(findTestObject('Object Repository/Page_Versions MyCollab/div_Settings'))

WebUI.click(findTestObject('Object Repository/Page_Project Settings MyCollab/label_None - You wont be notified of anythi_32514b'))

WebUI.click(findTestObject('Object Repository/Page_Project Settings MyCollab/span_Save'))

WebUI.closeBrowser()

