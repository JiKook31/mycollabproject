<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Some phase</name>
   <tag></tag>
   <elementGuidId>bec3f134-1b00-4832-a3c2-2929571ad4e6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='project-view-wrap']/div/div/div/div/div[2]/div[2]/div/div[2]/div/div/div/div/div/div/div/div/div/div[3]/div/div/div/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#project/milestone/preview/MTIvMw</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Some phase</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;project-view-wrap&quot;)/div[@class=&quot;v-expand&quot;]/div[@class=&quot;v-slot v-slot-vertical-tabsheet&quot;]/div[@class=&quot;v-customcomponent v-widget vertical-tabsheet v-customcomponent-vertical-tabsheet v-has-width&quot;]/div[@class=&quot;v-csslayout v-layout v-widget v-has-width&quot;]/div[@class=&quot;v-csslayout v-layout v-widget container-wrap v-csslayout-container-wrap main-content v-csslayout-main-content content-height v-csslayout-content-height v-has-width v-has-height&quot;]/div[@class=&quot;v-verticallayout v-layout v-vertical v-widget v-has-width v-has-height&quot;]/div[@class=&quot;v-expand&quot;]/div[@class=&quot;v-slot&quot;]/div[@class=&quot;v-horizontallayout v-layout v-horizontal v-widget v-has-width v-has-height v-margin-top v-margin-right v-margin-bottom v-margin-left&quot;]/div[@class=&quot;v-expand&quot;]/div[@class=&quot;v-slot&quot;]/div[@class=&quot;v-verticallayout v-layout v-vertical v-widget v-has-width&quot;]/div[@class=&quot;v-slot v-slot-roadmap-block&quot;]/div[@class=&quot;v-verticallayout v-layout v-vertical v-widget roadmap-block v-verticallayout-roadmap-block v-has-width v-margin-top v-margin-bottom&quot;]/div[@class=&quot;v-slot&quot;]/div[@class=&quot;v-horizontallayout v-layout v-horizontal v-widget v-has-width&quot;]/div[@class=&quot;v-expand&quot;]/div[@class=&quot;v-slot v-slot-editable-field&quot;]/div[@class=&quot;v-csslayout v-layout v-widget editable-field v-csslayout-editable-field v-has-width&quot;]/div[@class=&quot;v-label v-widget h3 v-label-h3 no-margin v-label-no-margin word-wrap v-label-word-wrap v-label-undef-w&quot;]/div[1]/a[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='project-view-wrap']/div/div/div/div/div[2]/div[2]/div/div[2]/div/div/div/div/div/div/div/div/div/div[3]/div/div/div/a</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Some phase')]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=''])[1]/following::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=''])[1]/preceding::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=''])[1]/preceding::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '#project/milestone/preview/MTIvMw')]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div/a</value>
   </webElementXpaths>
</WebElementEntity>
