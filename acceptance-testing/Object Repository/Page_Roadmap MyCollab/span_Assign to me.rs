<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Assign to me</name>
   <tag></tag>
   <elementGuidId>93c65b85-506d-4e2c-8610-db64c503ddee</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='ROOT-2521314-overlays']/div[3]/div/div/div[3]/div/div/div/div/div/div/div/div[3]/div/div/div/div[3]/div/div/div/div/div/div[3]/div/span/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>v-button-caption</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Assign to me</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ROOT-2521314-overlays&quot;)/div[@class=&quot;v-window v-widget v-has-width&quot;]/div[@class=&quot;popupContent&quot;]/div[@class=&quot;v-window-wrap&quot;]/div[@class=&quot;v-window-contents&quot;]/div[@class=&quot;v-scrollable&quot;]/div[@class=&quot;v-verticallayout v-layout v-vertical v-widget v-has-width v-margin-top v-margin-right v-margin-bottom v-margin-left&quot;]/div[@class=&quot;v-slot&quot;]/div[@class=&quot;v-csslayout v-layout v-widget v-has-width&quot;]/div[@class=&quot;v-verticallayout v-layout v-vertical v-widget form v-verticallayout-form v-has-width&quot;]/div[@class=&quot;v-slot v-slot-fixed v-slot-colored-gridlayout v-align-center&quot;]/div[@class=&quot;rl-container v-layout v-widget fixed rl-container-fixed colored-gridlayout rl-container-colored-gridlayout v-has-width&quot;]/div[@class=&quot;rl-row v-layout v-widget margin rl-row-margin&quot;]/div[@class=&quot;rl-col v-widget xs-12 rl-col-xs-12 sm-12 rl-col-sm-12 md-6 rl-col-md-6 lg-6 rl-col-lg-6&quot;]/div[@class=&quot;v-csslayout v-layout v-widget col-container v-csslayout-col-container&quot;]/div[@class=&quot;v-verticallayout v-layout v-vertical v-widget gridform-field v-verticallayout-gridform-field v-has-width v-margin-top v-margin-right v-margin-bottom v-margin-left&quot;]/div[@class=&quot;v-slot&quot;]/div[@class=&quot;v-horizontallayout v-layout v-horizontal v-widget v-has-width&quot;]/div[@class=&quot;v-expand&quot;]/div[@class=&quot;v-slot&quot;]/div[@class=&quot;v-customcomponent v-widget v-has-width&quot;]/div[@class=&quot;v-horizontallayout v-layout v-horizontal v-widget&quot;]/div[@class=&quot;v-slot v-slot-link&quot;]/div[@class=&quot;v-button v-widget link v-button-link&quot;]/span[@class=&quot;v-button-wrap&quot;]/span[@class=&quot;v-button-caption&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ROOT-2521314-overlays']/div[3]/div/div/div[3]/div/div/div/div/div/div/div/div[3]/div/div/div/div[3]/div/div/div/div/div/div[3]/div/span/span</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Assignee'])[1]/following::span[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='End Date'])[1]/following::span[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Status'])[1]/preceding::span[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=''])[1]/preceding::span[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div/div/div/div[3]/div/span/span</value>
   </webElementXpaths>
</WebElementEntity>
