import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.webui.contribution.WebUiDriverCleaner
import com.kms.katalon.core.mobile.contribution.MobileDriverCleaner
import com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner
import com.kms.katalon.core.windows.keyword.contribution.WindowsDriverCleaner


DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.windows.keyword.contribution.WindowsDriverCleaner())


RunConfiguration.setExecutionSettingFile('/var/folders/95/j0dq_vds7nz02sfxblbtzv640000gn/T/Katalon/20191027_161704/execution.properties')

TestCaseMain.beforeStart()

        TestCaseMain.runWSVerificationScript(new TestCaseBinding('',[:]), 'import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI\nimport com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile\nimport com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW\nimport com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS\nimport com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows\nimport static com.kms.katalon.core.testobject.ObjectRepository.findTestObject\nimport static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject\nimport static com.kms.katalon.core.testdata.TestDataFactory.findTestData\nimport static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase\nimport static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint\nimport com.kms.katalon.core.model.FailureHandling as FailureHandling\nimport com.kms.katalon.core.testcase.TestCase as TestCase\nimport com.kms.katalon.core.testdata.TestData as TestData\nimport com.kms.katalon.core.testobject.TestObject as TestObject\nimport com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint\nimport internal.GlobalVariable as GlobalVariable\nimport org.openqa.selenium.Keys as Keys\n\nWebUI.openBrowser(\'\')\n\nWebUI.navigateToUrl(\'http://localhost:8080/\')\n\nWebUI.setText(findTestObject(\'Object Repository/Page_MyCollab Setup Assistant Page/input_Server address (without port value an_77bc6d (2) (1)\'), \n    \'localhost\')\n\nWebUI.setText(findTestObject(\'Object Repository/Page_MyCollab Setup Assistant Page/input_Database name_databaseName (2) (1)\'), \n    \'mycollab\')\n\nWebUI.setText(findTestObject(\'Object Repository/Page_MyCollab Setup Assistant Page/input_User name_dbUserName (2) (1)\'), \n    \'root\')\n\nWebUI.setEncryptedText(findTestObject(\'Object Repository/Page_MyCollab Setup Assistant Page/input_Password_dbPassword (2) (1)\'), \n    \'loY/1845nitA0V8S+cELIQ==\')\n\nWebUI.setText(findTestObject(\'Object Repository/Page_MyCollab Setup Assistant Page/input_Database server address_databaseServer (2) (1)\'), \n    \'localhost:3306\')\n\nWebUI.click(findTestObject(\'Object Repository/Page_MyCollab Setup Assistant Page/button_Check Connection (2)\'))\n\nWebUI.click(findTestObject(\'Object Repository/Page_MyCollab Setup Assistant Page/button_Setup (2) (1)\'))\n\nWebUI.setText(findTestObject(\'Object Repository/Page_MyCollab - Online project management/input_Admin email_v-textfield v-widget v-ha_cd7b8e\'), \n    \'ilalova3198@gmail.com\')\n\nWebUI.setEncryptedText(findTestObject(\'Object Repository/Page_MyCollab - Online project management/input_Admin password_v-textfield v-widget v_749dc0\'), \n    \'yoXzwIskMKny5gwvAM7sBA==\')\n\nWebUI.click(findTestObject(\'Object Repository/Page_MyCollab - Online project management/span_Setup\'))\n\nWebUI.closeBrowser()\n\n', FailureHandling.STOP_ON_FAILURE, true)

